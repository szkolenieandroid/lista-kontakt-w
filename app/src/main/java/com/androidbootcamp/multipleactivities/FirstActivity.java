package com.androidbootcamp.multipleactivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class FirstActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
    }

    public void openSecondActivity(View view) {
        EditText inputText = (EditText) findViewById(R.id.input);
        String input = inputText.getText().toString();
        Intent secondActivityIntent = new Intent(this, SecondActivity.class);
        secondActivityIntent.putExtra("question", input);
        startActivityForResult(secondActivityIntent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TextView responseText = (TextView) findViewById(R.id.response);
        responseText.setText(data.getStringExtra("response"));
    }
}
